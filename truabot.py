#!/usr/bin/env python3
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, MessageHandler, Filters, ConversationHandler
from telegram import InlineKeyboardMarkup, InlineKeyboardButton, KeyboardButton, ReplyKeyboardMarkup, ReplyKeyboardRemove, error, ChatAction
from Suggestions import Suggestions
from City import City
from Temp import Temp
from Tratte import Tratte
import calendar
import datetime


import logging
updater = Updater(token="insert your token", use_context=True)
dispatcher = updater.dispatcher
# logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger()
logger.setLevel(logging.INFO)

cityes = {}

def cityclear(user_id):
    global cityes
    cityes[user_id] = []

def cityesadd(string,user_id):
    global cityes
    if type(string) is str:
        data = string.split("/")
        suggestions = Suggestions(str(data[0]))
        suggestions = suggestions.__getattribute__("suggestions")
        c = City(str(suggestions[int(data[1])][0]), str(suggestions[int(data[1])][1]))
        cityes[user_id].append(c)
    elif type(string) is datetime.datetime:
        cityes[user_id].append(str(string))
    print(cityes[user_id])

def create_callback_data(action,year,month,day):
    """ Create the callback data associated to each button"""
    return ";".join([action,str(year),str(month),str(day)])

def separate_callback_data(data):
    """ Separate the callback data"""
    return data.split(";")


def create_calendar(year=None, month=None):
    """
    Create an inline keyboard with the provided year and month
    :param int year: Year to use in the calendar, if None the current year is used.
    :param int month: Month to use in the calendar, if None the current month is used.
    :return: Returns the InlineKeyboardMarkup object with the calendar.
    """
    now = datetime.datetime.now()
    if year == None: year = now.year
    if month == None: month = now.month
    data_ignore = create_callback_data("IGNORE", year, month, 0)
    keyboard = []
    #First row - Month and Year
    row=[]
    row.append(InlineKeyboardButton(calendar.month_name[month]+" "+str(year),callback_data=data_ignore))
    keyboard.append(row)
    #Second row - Week Days
    row=[]
    for day in ["Mo","Tu","We","Th","Fr","Sa","Su"]:
        row.append(InlineKeyboardButton(day,callback_data=data_ignore))
    keyboard.append(row)

    my_calendar = calendar.monthcalendar(year, month)
    for week in my_calendar:
        row=[]
        for day in week:
            if(day==0):
                row.append(InlineKeyboardButton(" ",callback_data=data_ignore))
            else:
                row.append(InlineKeyboardButton(str(day),callback_data=create_callback_data("DAY",year,month,day)))
        keyboard.append(row)
    #Last row - Buttons
    row=[]
    row.append(InlineKeyboardButton("<",callback_data=create_callback_data("PREV-MONTH",year,month,day)))
    row.append(InlineKeyboardButton(" ",callback_data=data_ignore))
    row.append(InlineKeyboardButton(">",callback_data=create_callback_data("NEXT-MONTH",year,month,day)))
    keyboard.append(row)

    return InlineKeyboardMarkup(keyboard)


def process_calendar_selection(update, context):
    logger.log(logging.INFO,
               "user_id: " + str(update.callback_query.message.chat.id) + " username " + str(update.callback_query.message.chat.username) +
               " " + str(update.callback_query.data))
    """
    Process the callback_query. This method generates a new calendar if forward or
    backward is pressed. This method should be called inside a CallbackQueryHandler.
    :param telegram.Bot bot: The bot, as provided by the CallbackQueryHandler
    :param telegram.Update update: The update, as provided by the CallbackQueryHandler
    :return: Returns a tuple (Boolean,datetime.datetime), indicating if a date is selected
                and returning the date if so.
    """
    ret_data = (False,None)
    query = update.callback_query
    (action, year, month, day) = separate_callback_data(query.data)
    curr = datetime.datetime(int(year), int(month), 1)
    if action == "IGNORE":
        return "Calendar"
    elif action == "DAY":
        ret_data = datetime.datetime(int(year), int(month), int(day))
        if datetime.datetime.strptime(str(datetime.datetime.now().date()), "%Y-%m-%d") > ret_data:
            return "Calendar"
        else:
            cityesadd(ret_data, query.message['chat']['id'])
            button_list = []
            row = [InlineKeyboardButton("AM", callback_data="am"), InlineKeyboardButton("PM",callback_data="pm")]
            button_list.append(row)
            reply_markup = InlineKeyboardMarkup(button_list)
            query.edit_message_text(text="Scegli l'ora di partenza", reply_markup=reply_markup)
            return "AmPm"
    elif action == "PREV-MONTH":
        pre = curr - datetime.timedelta(days=1)
        query.edit_message_text(text=query.message.text, reply_markup=create_calendar(int(pre.year),int(pre.month)))
        return "Calendar"
    elif action == "NEXT-MONTH":
        ne = curr + datetime.timedelta(days=31)
        query.edit_message_text(text=query.message.text, reply_markup=create_calendar(int(ne.year),int(ne.month)))
        return "Calendar"
    else:
        context.answer_callback_query(callback_query_id= query.id, text="qualcosa e' andato storto!")
        # UNKNOWN
    return ConversationHandler.END


def getinitialinlinekeyboard():
    reply_markup = [[InlineKeyboardButton("Help", callback_data="Help"),
                     InlineKeyboardButton("Bus", callback_data="Bus")]]
    reply_markup = InlineKeyboardMarkup(reply_markup)
    return reply_markup


def start(update, context):
    txt = "Benvenuto in Truabot:\nQui potrai controllare gli orari degli " \
          "autobus TUA in maniera comoda e " \
          "senza aprire il Lento sito web\nhttps://tua.mycicero.it/TPWebPortal/"
    custom_keyboard = [[KeyboardButton("Help"), KeyboardButton("Bus")]]
    reply_markup = ReplyKeyboardMarkup(custom_keyboard)
    context.bot.send_message(chat_id=update.message.chat_id, text=txt, reply_markup=reply_markup)

'''
 def help1cq(update, context):
    query =update.callback_query
    txt = "Command List:\n" \
          "/help :   Richiama questo messaggio di help\n" \
          "/bus  :   Cerca tratte"
    try:
        query.edit_message_text(text=txt, reply_markup=getinitialinlinekeyboard())
    except error.BadRequest:
        pass
'''


def cancel(update, context):
    update.message.reply_text("Operazione annullata")
    return ConversationHandler.END


def help1(update, context):
    txt = "Command List:\n" \
          "/help :   send help\n" \
          "/bus  :   query bus server\n" \
          "/cancel : cancel current query"
    context.bot.send_message(chat_id=update.message.chat_id, text=txt)


def bus(update, context):
    logger.log(logging.INFO,"user_id: " + str(update.message.chat_id) + " username " + str(update.message.chat.username) +
    "command /bus")
    cityclear(update.message.from_user.id)
    context.bot.send_message(chat_id=update.message.chat_id, text="da dove vuoi partire?")
    return "Startpoint"


def startpoint(update, context):
    logger.log(logging.INFO,
               "user_id: " + str(update.message.chat_id) + " username " + str(update.message.chat.username) +
                " " + str(update.message.text))
    try:
        suggestions = Suggestions(str(update.message.text))
        suggestions = suggestions.__getattribute__("suggestions")
        y = 0
        button_list= []
        for x in suggestions:
            button_list.append([InlineKeyboardButton(x[0], callback_data=str(update.message.text) + "/" + str(y) + "/")])
            y = y+1
        reply_markup = InlineKeyboardMarkup(button_list)
        context.bot.send_message(chat_id=update.message.chat_id, text="Scegli la citta'", reply_markup=reply_markup)
    except ValueError:
        context.bot.send_message(chat_id=update.message.chat_id, text="Nome citta' non valido o inesistente")
        return ConversationHandler.END 
    return "Startpointcq"


def startpointcq(update, context):
    update = update.callback_query
    logger.log(logging.INFO,
               "user_id: " + str(update.message.chat.id) + " username " + str(update.message.chat.username) +
               " " + str(update.data))
    cityesadd(update.data, update.message['chat']['id'])
    update.edit_message_text(text="dove vuoi arrivare?", reply_markup=None)
    return "Endpoint"


def endpoint(update, context):
    logger.log(logging.INFO,
               "user_id: " + str(update.message.chat_id) + " username " + str(update.message.chat.username) +
               " " + str(update.message.text))
    try:
        suggestions = Suggestions(str(update.message.text))
        suggestions = suggestions.__getattribute__("suggestions")
        y = 0
        button_list = []
        for x in suggestions:
            button_list.append(
                [InlineKeyboardButton(x[0], callback_data=str(update.message.text) + "/" + str(y) + "/")])
            y = y + 1
        reply_markup = InlineKeyboardMarkup(button_list)
        context.bot.send_message(chat_id=update.message.chat_id, text="Scegli la citta'", reply_markup=reply_markup)
    except ValueError:
        context.bot.send_message(chat_id=update.message.chat_id, text="Nome citta' non valido o inesistente")
        return ConversationHandler.END
    return "Endpointcq"


def endpointcq(update, context):
    update = update.callback_query
    logger.log(logging.INFO,
               "user_id: " + str(update.message.chat.id) + " username " + str(update.message.chat.username) +
               " " + str(update.data))
    cityesadd(update.data, update.message['chat']['id'])
    update.edit_message_text(text="Scegli la data: ", reply_markup=create_calendar())
    return "Calendar"

def ampm(update, context):
    update = update.callback_query
    logger.log(logging.INFO,
               "user_id: " + str(update.message.chat.id) + " username " + str(update.message.chat.username) +
               " " + str(update.data))
    button_list = []
    if update.data == "am":
        z = 0
    elif update.data == "pm":
        z = 13
    for x in range(0, 3):
        temp = []
        for y in range(0, 4):
            if z == 0:
                temp.append(InlineKeyboardButton(str(z),callback_data="0" + str(z) + ":00:00"))
                z = z+1
            if z < 10:
                temp.append(InlineKeyboardButton(str(z), callback_data="0" + str(z) + ":00:00"))
            else:
                temp.append(InlineKeyboardButton(str(z), callback_data=str(z) + ":00:00"))
            z = z+1
        button_list.append(temp)
    reply_markup = InlineKeyboardMarkup(button_list)
    update.edit_message_text(text="Scegli l'ora: ", reply_markup=reply_markup)
    return "Hours"

def hours(update, context):
    global cityes
    update = update.callback_query
    logger.log(logging.INFO,
               "user_id: " + str(update.message.chat.id) + " username " + str(update.message.chat.username) +
               " " + str(update.data))
    hour = update.data
    if datetime.datetime.now().date() == datetime.datetime.strptime(cityes[update.message['chat']['id']][2], "%Y-%m-%d %H:%M:%S").date():
        if datetime.datetime.now().hour > datetime.datetime.strptime(hour, "%H:%M:%S").hour:
            return "Hours"
    starthour = datetime.datetime.strptime(str(datetime.datetime.now().date()) + " " + hour, "%Y-%m-%d %H:%M:%S")
    cityesadd(starthour, update.message['chat']['id'])
    dataarray = cityes[update.message['chat']['id']]
    update.edit_message_text(text="Queryng server☁️, attendere qualche secondo...")
    context.bot.send_chat_action(chat_id=update.message['chat']['id'], action=ChatAction.TYPING) 
    tratte = Tratte(dataarray[0], dataarray[1], dataarray[2], dataarray[3])
    tratte = tratte.__getattribute__("tratte")
    if tratte == []:
        update.edit_message_text(text="Mi spiace, non ci sono corse disponibili :(")
    else:
        finallyfinalmessage = ""
        for x in tratte:
            finallyfinalmessage += "METRI=" + '\"'+ str(x[0]) + '\"      ' + " MINUTI=" + '\"'+ str(x[1]) + '\"      ' + " CAMBI=" + '\"'+ str(x[2]) + '\"\n'
            for y in x[3]:
                finallyfinalmessage += "SALITA=" + '\"'+ str(y[0]) + '\"' + "( "+ str(y[2])[-8:] +")" + "\n" + "DISCESA=" + '\"'+ y[1] + '\"' + "( "+ str(y[3])[-8:] +")\n"
            finallyfinalmessage += "################################################\n\n"
            update.edit_message_text(text=finallyfinalmessage)
    return ConversationHandler.END
# handlers
start_handler = CommandHandler("start", start)
help_handler = MessageHandler(Filters.regex("/help|Help"), help1)
bus_handler = ConversationHandler(
    entry_points=[MessageHandler(Filters.regex("/bus|Bus"), bus)],

    states={
        "Startpoint": [MessageHandler(Filters.text, startpoint)],
        "Startpointcq": [CallbackQueryHandler(startpointcq)],
        "Endpoint": [MessageHandler(Filters.text, endpoint)],
        "Endpointcq": [CallbackQueryHandler(endpointcq)],
        "Calendar": [CallbackQueryHandler(process_calendar_selection)],
        "AmPm": [CallbackQueryHandler(ampm)],
        "Hours": [CallbackQueryHandler(hours)]
    },

    fallbacks=[CommandHandler('cancel', cancel)]
)

dispatcher.add_handler(start_handler)
dispatcher.add_handler(help_handler)
dispatcher.add_handler(bus_handler)
updater.start_polling()
