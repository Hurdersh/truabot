import requests
# class used to obtain and store information about city from gmaps


class City:
    # string that identify the city and placeid, everytwo are strings
    def __init__(self, cityname, place_id):
        self.cityname = cityname
        self.place_id = place_id
        # check if attributes have correct type, if they are invalid
        # raise a ValueError
        if type(self.cityname) is not str:
            raise ValueError("invalid cityname")
        if type(self.place_id) is not str:
            raise ValueError("invalid place_id")
        url = "https://tua.mycicero.it/OTPProxy/gmapPlacesHandler.ashx?" \
              "req=details&placeid=" + place_id + "&fields=address_component,geometry" \
              "&sessiontoken=706cbeab-7c7d-4de5-866d-d9b20d7ddbe0&language=en"
        # requests.exceptions.ConnectionError is raised if it is a bad connection
        geometry = (requests.post(url).json())["result"]["geometry"]["location"]
        self.latitude = geometry["lat"]
        self.longitude = geometry["lng"]

