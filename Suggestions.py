import requests
# class used to retrive suggestions from db server


class Suggestions:
    # name is a string used to query suggestion db
    def __init__(self, name):
        # check if name is correct type
        if type(name) is not str:
            raise ValueError("name isn't valid")
        c = 0
        self.suggestions = []
        # check if name have only spaces and alphabetical
        while c < name.__len__() and (name[c].isalpha() or name[c].isspace()):
            c = c+1
        if not c == name.__len__():
            # if name isn't only chars and spaces raise ValueError
            raise ValueError("name isn't valid")
        # url used to query database of tua for retrieve data,the only variable is the name of city autocomplete
        url = "https://tua.mycicero.it/OTPProxy/gmapPlacesHandler.ashx" \
              "?req=autocomplete&input=" + name + \
              "&components=country:it&sessiontoken=c3bb614b-f4d3-4f97-b814-f70d8c8ba973" \
              "&location=42.35,13.854&radius=120000"
        # requests.exceptions.ConnectionError is raised if it is a bad connection
        suggestions = requests.get(url=url).json()
        # check if result of array is zero
        if suggestions["status"] == "ZERO_RESULTS":
            raise ValueError("city doesn't exist")
        # creation of suggestion list of lists when lista = [[descrizione,placeid]]
        for x in suggestions["predictions"]:
            self.suggestions.append([x["description"], x["place_id"]])
