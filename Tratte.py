import requests
import datetime
from City import City
import json


class Tratte:
    # date of start and starthour(datetime in format %Y-%m-%d %H:%M:%S),City and City
    def __init__(self, fromcity, destinationcity, startdate, starthour):
        # check if attributes have correct type, if they are invalid
        # raise a ValueError
        if type(startdate) is not str:
            raise ValueError("invalid startname, give a str with format %Y-%m-%d %H:%M:%S")
        if type(starthour) is not str:
            raise ValueError("invalid starthour, give a str with format %Y-%m-%d %H:%M:%S")
        if type(fromcity) is not City:
            raise ValueError("invalid City")
        if type(destinationcity) is not City:
            raise ValueError("invalid City")
        url = "https://tua.mycicero.it/OTPProxy/host.ashx?url=momoservice/json/FindTPSolutions"
        # organized in list [[metritotali, minutitotali, cambi, ]]
        self.tratte = []

        # core of the request
        tratte = requests.post(url=url, json={"DataPartenza": self.__serialize_date__(startdate),
                                         "Intermodale": "false",
                                         "PuntoDestinazione": {"Formato": 0, "Lat": destinationcity.__getattribute__("latitude"),
                                                               "Lng": destinationcity.__getattribute__("longitude")},
                                         "OraDa": self.__serialize_date__(starthour),
                                         "PuntoOrigine": {"Formato": 0, "Lat": fromcity.__getattribute__("latitude"),
                                                          "Lng": fromcity.__getattribute__("longitude")},
                                         "TipoPercorso": 0,
                                         "NumMaxSoluzioni" : 5
                                         })
        data = tratte.json()["Oggetti"]
        if len(data) == 0:
            #if data == [] tratte will be []
            data = []

        # n^2 is necessary
        for x in data:
            temp = []
            temp.append(x['MetriTotali'])
            temp.append(x['MinutiTotali'])
            temp.append(x['NumeroCambi'])
            temp2 = []
            for y in x['Tratte']:
                temp1 = []
                temp1.append(y["LocalitaSalita"]["Descrizione"])
                temp1.append(y["LocalitaDiscesa"]["Descrizione"])
                temp1.append(self.__deserialize_date__(y["OrarioPartenza"]))
                temp1.append(self.__deserialize_date__(y["OrarioArrivo"]))
                temp2.append(temp1)
            temp.append(temp2)
            # self.tratte = [ [Metritotali,MinutiTotali,NumeroCambi, [[tratta1], [tratta2]]],
            # [Metritotali,MinutiTotali,NumeroCambi, [[tratta1], [tratta2]]] ]
            self.tratte.append(temp)

    def isempty(self):
        if len(self.tratte) == 0:
            return True
        return False

    # method to serialize json .NET date format(transpose all in ms)
    @staticmethod
    def __serialize_date__(datestring):
        datestring = datetime.datetime.strptime(datestring, "%Y-%m-%d %H:%M:%S")
        return "/Date(" + str(datestring.timestamp() * 1000)[:-2] + "+0200)/"

    # method to deserialize json .NET date format(transpose all in ms)
    @staticmethod
    def __deserialize_date__(datestring):
        timepart = datestring.split('(')[1].split(')')[0]
        milliseconds = int(timepart[:-5])
        hours = int(timepart[-5:]) / 100
        adjustedseconds = milliseconds / 1000 + hours * 3600
        return str(datetime.datetime.utcfromtimestamp(0) + datetime.timedelta(seconds=adjustedseconds))[-8:-3]

